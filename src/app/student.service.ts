import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import { Student } from './student';
import { Observable } from 'rxjs';
import { Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private baseUrl = 'http://localhost:9898/springboot-crudrest/rest/student';

  constructor(private http:HttpClient) {
    
   }
   getAllStudents():Observable<Student[]>{
    return this.http.get<Student[]>(`${this.baseUrl}/all`);
   }

   createStudent(student : Student):Observable<Message>{
    return this.http.post<Message>(`${this.baseUrl}/save`,student);
    }
    deleteStudent(stdId:number):Observable<Message>{
      console.log("Delete Service"+`${this.baseUrl}/remove/${stdId}`);
      return this.http.delete<Message>(`${this.baseUrl}/remove/${stdId}`);
      }
}
