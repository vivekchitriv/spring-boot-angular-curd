import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  
  students : Student[];
  message: import("d:/Angula by vivek/Apps/boot-student-app/src/app/message").Message;
  
  constructor(private service:StudentService, private router:Router) { }

  ngOnInit(): void {
    this.getAllStudents();
  }
  getAllStudents(){
    this.service.getAllStudents().subscribe(data=>{this.students=data});
    }
    deleteStudent(stdId:number){
      console.log("Delete data"+stdId);
      this.service.deleteStudent(stdId).subscribe(data=>{
      this.message=data,
      this.getAllStudents();
      });
     }
}
