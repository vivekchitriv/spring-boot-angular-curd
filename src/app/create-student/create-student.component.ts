
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from '../message';
import { Student } from '../student';
import { StudentService } from '../student.service';


@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent implements OnInit {

  // stdId : number;
  //   stdName : string;
  //   stdFee : number;
  //   stdCourse : string;
  
  public student : Student = new Student();
  public message : Message = new Message();
  
  

  error : boolean ;
  constructor(private service:StudentService, private router:Router) { }

  ngOnInit(): void {
  }

  saveStudent(){

    console.log(this.student);
    this.service.createStudent(this.student)
    .subscribe(data =>{ this.message=data},
    error=>this.message=error);
    this.student=new Student();
    if(this.message.type=='FAIL') this.error=true;
    //this.gotoViewAll();
    }
    /*
 gotoViewAll(){
 this.router.navigate(['/students']);
 }
 */


  

}
